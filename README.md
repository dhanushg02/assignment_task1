#Form Submission App using Node:#
This Node.js application is a simple form submission app built using Express.js. It allows users to submit their personal information through a web form and stores the entered data in a JSON file named form-inputs.json. Users can also view the submitted details on a separate page.

#Features#
1.Form Submission: Users can submit their first name, last name, age, and email address through a web form.
2.Data Storage: Submitted form data is stored in a JSON file (form-inputs.json) located in the project directory.

#File Structure#
1.app.js: Entry point of the application where Express.js routes are defined.
2.form-inputs.json: JSON file to store submitted form data.
3.README.md: Documentation explaining how to use the application.

#Technologies Used#
1.Node.js: JavaScript runtime environment.
2.Express.js: Web application framework for Node.js.
3.fs: Node.js file system module for file operations.
